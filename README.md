Aplicación de creación de archivos con tablas de multiplicar construída en NodeJS 16.15.0

1. git clone
2. npm i para instalar dependencias
3. node -h para ayuda relacionada a los comandos necesarios

Ejemplo de como crear un archivo con la tabla de multiplicar deseada:

"node app -b 5 -h 10"

Devolverá un archivo con la tabla del 5, hasta el 10. Si deseas ver el resultado en consola agrega la variable "-l"

Humberto Domizi
