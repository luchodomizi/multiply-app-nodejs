const fs = require("fs");

require("colors");

const crearArchivo = async (base = 5, listar, hasta = 10) => {
  try {
    let salida = "";
    let consola = "";

    for (let index = 1; index <= hasta; index++) {
      salida += `${base} x ${index} = ${base * index}\n`;
      consola += `${base}${" x ".green}${index}${" = ".green}${base * index}\n`;
    }

    fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);

    if (listar) {
      console.log("=======================".green);
      console.log(`Tabla del: ${base}`.bold.green.underline);
      console.log("=======================".green);
      console.log(consola);
    }

    return `tabla-${base}.txt`;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  crearArchivo,
};
