const argv = require("yargs")
  .option("b", {
    alias: "base",
    type: "number",
    default: 5,
    demandOption: true,
    describe: "Es la base de la tabla de multiplicar",
  })
  .option("h", {
    alias: "hasta",
    type: "number",
    default: 10,
    demandOption: true,
    describe: "Es el límite hasta donde quieres que multiplique",
  })
  .option("l", {
    alias: "listar",
    type: "boolean",
    default: false,
    describe: "Muestra la tabla en consola",
  })
  .check((argv, options) => {
    if (isNaN(argv.b)) {
      throw "La base debe ser un número";
    }
    if (isNaN(argv.h)) {
      throw "El valor de 'hasta' debe ser un número";
    }
    return true;
  }).argv;

module.exports = argv;
