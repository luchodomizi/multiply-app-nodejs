const { crearArchivo } = require("./helpers/multiplicar");
const argv = require("./config/yargs");

crearArchivo(argv.base, argv.l, argv.hasta)
  .then((archivo) => {
    console.log(`El archivo ${archivo.underline.green} ha sido creado`);
  })
  .catch((err) => console.log(err));
